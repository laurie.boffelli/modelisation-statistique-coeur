liste = dir(['HTN*.txt']);
%J=[2,4,15,16,18];
X=[0:1/24:1];
Y=zeros(1,25);
C=xlsread('points courbes.xls');
v=1;
N=1;
for i=30:length(liste)%length(J)
    filename=strcat(liste(i).name);%strcat(liste(J(i)).name);
    L=load(filename);
    for j=1:25
        Y(1,j)=L(j)/max(L);
    end
    P=figure()
    plot(X,Y,'.-')
    for w=1:25
        text(X(w),Y(w),int2str(w))
    end
    hold on
    plot(X(C(v,1)),Y(C(v,1)),'o','Linewidth',1.2)
    plot(X(C(v,2)),Y(C(v,2)),'o','Linewidth',1.2)
    %plot(X(C(v,3)),Y(C(v,3)),'o','Linewidth',1.2)
    if C(v,3)~=0
        %title("Courbe avec plateau")
        plot(X(C(v,3)),Y(C(v,3)),'o','Linewidth',1.2)
    %else
        %title("Courbe sans plateau")
    end
    hold off
    
    %recherche avec dérivée
    %figure()
    %dy=gradient(Y(:))./gradient(X(:));
    %plot(X, dy)
    
    %sauvegarder image
    if N~=3 & N~=12
        saveas(P,strcat('CSN',int2str(N),'.png'))
    else
        N=N+1
        saveas(P,strcat('CSN',int2str(N),'.png'))
    end
    v=v+1;
    N=N+1
end
%plot(X,Y)
%C(ligne,colonne)


