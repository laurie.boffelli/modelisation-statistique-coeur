function [F_mesure,M_seuil]=find_seuil_score()
    Seuil=[-9:0.1:1];
    diagn=[true,false,true,false,false,true,false,false,false,false,true,false,true,false,false,true,false,false,false,false,true,false,false,true,true,true,true,false,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true];
    a=1;
    listept=dir(['LLLDyn*.txt']);
    X=zeros(1,47);
    Y=zeros(1,47);
    %dist1=2^(1/2);
    F_mesure1=0;
    while Seuil(a)~=1
        seuil=Seuil(a);
        [TN,TP,FN,FP]=TN_TP_FN_FP(listept,seuil,diagn);
        TPR=TP/(TP+FN);
        Y(1,a)=TPR;
        FPR=FP/(FP+TN);
        X(1,a)=FPR;
        %dist2=((X(1,a))^2+((Y(1,a))-1)^2)^(1/2);
        %dist1
        %a
        
        Prec=TP/(TP+FP);
        Rap=TP/(TP+FN);
        F_mesure2=(2*Prec*Rap)/(Prec+Rap);
        
        if F_mesure2>F_mesure1
            %a
            F_mesure1=F_mesure2;
            M_seuil=Seuil(a);
        end
        
        a=a+1;
    end

    F_mesure=F_mesure1

    M_seuil
    %Seuil(81);
    plot(X,Y);
    %(1,82);
    %X(1,82);
end