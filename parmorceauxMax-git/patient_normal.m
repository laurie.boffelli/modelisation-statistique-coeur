function normal=patient_normal(pt,seuil)
    N=0;
	A=0;
	i=1;
    PT=load(pt);
	while i<=25
        if PT(i)>=seuil
            N=N+1;
        else
             A=A+1;
        end
        i=i+1;
    end
	if A>0
        normal=false;
    else
        normal=true;
	end
end
