%F_Mesure=0;
%M_Seuil=0;
%for initial_alpha=0.035:0.001:0.043%0.035:0.001:0.045%0.01:0.01:0.1%0.05:0.05
    %% liste of testing data
    liste = dir(['HTN*.txt']);
    save testtest1.mat
    save testtest2.mat
    save LLLDynWin.mat
    save LLLDyn.mat
    %% initialisation
    % set display to -1 to disable
    display = 1;%1(ancienne version)%7;%-1
    interface = Interface({'interpolants_A', 'interpolants_B1', 'interpolants_B2', 'interpolants_C'}, display);
    % set initial_alpha = -1 to have it estimated automatically
    initial_alpha =0.037% 0.038 %0.04;%0.05 %0.025;(0.039)
    sigma_estimation = 7e-3;
    sigma_test = 1e-3;
    threshold_dynamics = -0.7;
    %% testing
    J=[5]%[8,29,42,45,46]%J=[1,3,6,11,13,16,21,24,25,26,27,29];
   
    for i=1:length(J)%length(J)%length(liste)
        filename=strcat(liste(J(i)).name)%liste(i).name%liste(J(i)).name
        interface.process_new_sequence(filename, [initial_alpha*2.1 initial_alpha*2 initial_alpha*1.5 initial_alpha*2.4], sigma_estimation, sigma_test, threshold_dynamics);
        %j'essaie de "save" les meilleurs sequences
        testtest1=interface.meilleure_sequence
        if (i==1)
            bestSequence=testtest1;
        end
        if (i>1)
            bestSequence=[bestSequence;testtest1];
        end

        LLLDyn=interface.loglikelihood


%         testtest1=interface.loglikelihood_dynamics_window(26:50);
%         testtest2=interface.loglikelihood_dynamics(26:50);
%         if (i==1)
%             LLLDynWin=testtest1;
%             LLLDyn=testtest2;
%         end
%         if (i>1)
%             LLLDynWin=[LLLDynWin;testtest1];
%             LLLDyn=[LLLDyn;testtest2];
%         end
        
    end
    %figure()
    %plot([1:25],LLLDynWin)
    figure()
    plot([1:25],LLLDyn)
    %
    
    for i=1:length(liste)%length(J)%length(liste)
        if (i < 10)
            name = strcat('0',int2str(i));
        else
            name = int2str(i);
        end
        
        dlmwrite(strcat('LLLDynpt',name,'.txt'),LLLDyn(i,:));
    end   
    %[f_mesure,m_seuil]=find_seuil_score()
    %if f_mesure>F_Mesure
        %F_Mesure=f_mesure
        %optinitial_alpha=initial_alpha
        %M_Seuil=m_seuil
    %end
%end
%F_Mesure
%M_Seuil
%optinitial_alpha
