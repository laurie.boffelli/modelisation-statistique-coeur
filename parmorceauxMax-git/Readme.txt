This is the release version 1.0 of our movement quality assessment code. For information on the methods, please refer to http://www.irc-sphere.ac.uk/work-package-2/movement-quality and to our publications.


**************************************************************
CONDITIONS OF USE

Copyright (c) 2014, Adeline Paiement, Lili Tao
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

    * The two following publications must be cited in published works
      that use or incorporate this software:
	Adeline Paiement, Lili Tao, Sion Hannuna, Massimo Camplani, Dima Damen, Majid Mirmehdi,
	Online quality assessment of human movement from skeleton data.
	Proceedings of British Machine Vision Conference (BMVC), September 2014.
	
	Lili Tao, Adeline Paiement, Dima Damen, Sion Hannuna, Massimo Camplani, Tilo Burghardt, Ian Craddock,
	A Comparative Study of Pose Representation and Dynamics Modelling for Online Motion Quality Assessment.
	Submitted to CVIU - under review.
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of the University of Bristol, the Sphere project,
      nor the names of its contributors may be used to endorse or promote
      products derived from this software without specific prior written
      permission.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.


Disclaimer: this software is NOT for clinical use.
The movement quality assessment software is intended for educational, research, and informational purposes only. It may be used only for these purposes and may not under any circumstances be used for clinical purposes.

This software is made available with no warranties.

To acknowledge this movement quality assessment software as a platform, please cite the project web site (http://www.irc-sphere.ac.uk/work-package-2/movement-quality) and the following publications when publishing work that uses or incorporates it.

- Adeline Paiement, Lili Tao, Sion Hannuna, Massimo Camplani, Dima Damen, Majid Mirmehdi, Online quality assessment of human movement from skeleton data. Proceedings of British Machine Vision Conference (BMVC), September 2014.

- Lili Tao, Adeline Paiement, Dima Damen, Sion Hannuna, Massimo Camplani, Tilo Burghardt, Ian Craddock, A Comparative Study of Pose Representation and Dynamics Modelling for Online Motion Quality Assessment. Submitted to CVIU - under review.


**************************************************************
INSTALL (Matlab code tested on Windows only, and Python code tested on Linux-Ubuntu only)

No compilation is required.

* Dependencies

The following librairies are required for the Python code:

numPy
scipy
scikit-learn


**************************************************************
USAGE

Train (Matlab and Python):
1) run the matlab script prepare_models_part1 -- you may use the script "example_prepare_models.m" as an example of how to call prepare_models_part1.
2) run the Python script pdf_estimations.py
3) run the matlab script prepare_models_part2

Using trained models (matlab):
1) Instantiate matlab class Interface
2) call Interface.process_new_sequence
You may use the script "example_test_models.m" as a usage example.

Note: this code is designed to use OpenNI skeletons. Kinect skeletons may be used by modifying the Interface.read_data function and the script prepare_models_part1.

File format of the OpenNI skeletons:
These files contain the 15 OpenNI joints, with x,y,z coordinates for each joint.
Each line contains one frame, in the following format:
[frame number] [JOINT_HEAD] [JOINT_NECK] [JOINT_LEFT_SHOULDER] [JOINT_RIGHT_SHOULDER] [JOINT_LEFT_ELBOW] [JOINT_RIGHT_ELBOW] [JOINT_LEFT_HAND] [JOINT_RIGHT_HAND] [JOINT_TORSO] [JOINT_LEFT_HIP] [JOINT_RIGHT_HIP] [JOINT_LEFT_KNEE] [JOINT_RIGHT_KNEE] [JOINT_LEFT_FOOT] [JOINT_RIGHT_FOOT]

The format of JOINT_X is:
[flag (can be ignored)] [x] [y] [z]