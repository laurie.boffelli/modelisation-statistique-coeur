function [TN,TP,FN,FP]=TN_TP_FN_FP(listept,seuil,diagn) 
    i=1;
    TN=0;
    TP=0;
    FN=0;
    FP=0;
    while i<=length(listept)
        pt=listept(i).name;
        if patient_normal(pt,seuil)==true
            if patient_normal(pt,seuil)==diagn(i)
                TN=TN+1;
            else
                FN=FN+1;
            end
        end
        if patient_normal(pt,seuil)==false
            if patient_normal(pt,seuil)==diagn(i)
                TP=TP+1;
            else
                FP=FP+1;
            end
        end
        i=i+1;
    end
end
