classdef ROC_curve<handle
    properties
        normal
        TN
        TP
        FN
        FP
    end
        function normal=patient_normal(pt,seuil)
            N=0
            A=0
            i=1
            while i<=25
                if pt(i)>=seuil
                    N=N+1
                else
                    A=A+1
                end
                i=i+1
            end
            if A>0
                normal='False'
            else
                normal='True'
            end
        end

        function [TN,TP,FN,FP]=TN_TP_FN_FP(pt,seuil,diagn)
            i=1
            while i<=29
                if patient_normal(pt,seuil)==True
                    if patient_normal(pt,seuil)==diagn(i)
                        TN=TN+1
                    else
                        FN=FN+1
                    end
                end
                if patient_normal(pt,seuil)==False
                    if patient_normal(pt,seuil)==diagn(i)
                        TP=TP+1
                    else
                        FP=FP+1
                    end
                end
                i=i+1
            end
        end
end
%diagn=[True,False,True,False,False,True,False,False,False,False,True,False,True,False,False,True,False,False,False,False,True,False,False,True,True,True,True,False,True]


