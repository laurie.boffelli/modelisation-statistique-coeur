% build dynamics model (part 1)
%max
Y=load('ListeDesVolumes.txt');
y_training=Y;
y_training=zeros(18*25,1);
J=1;
I=1;
i=1;
while (I<=18);
    J=1;
    while (J<=25);
        y_training(i,1)=Y(I,J);
        i=i+1;
        J=J+1;
    end
    I=I+1;
end
%on cherche le Y_max de chaque patient
 Y_max=[];
 y_max=0;
 k=1;
 i=1;
 while(k<=18);
     y_max=0;
     while(i<=25*k);
         if (y_training(i,1)>y_max);
             y_max=y_training(i,1);
         end
         i=i+1;  
     end
     y_max=repmat(y_max,1,25);
     Y_max=[Y_max,y_max];
     k=k+1;
 end
Y_max=Y_max'; 
 %maintenant on va multiplier chaque élément de Y_training par 1/Y_max
 %pour avoir le poucentage

i=1;
Y_training=zeros(18*25,1);
while (i<=450);
    Y_training(i,1)=y_training(i,1)*(1/Y_max(i,1));
    i=i+1;
end
%creation de X_training
X=[0:1/24:1];
X_training=X';
X_training=repmat(X_training,18,1);


%min-max
% Y=load('ListeDesVolumes.txt');
% y_training=Y;
% y_training=zeros(18*25,1);
% J=1;
% I=1;
% i=1;
% while (I<=18);
%     J=1;
%     while (J<=25);
%         y_training(i,1)=Y(I,J);
%         i=i+1;
%         J=J+1;
%     end
%     I=I+1;
% end
% %on cherche le Y_max de chaque patient
% %on cherche le Y_min de chaque patient 
%  Y_max=[];
%  y_max=0;
%  Y_min=[];
%  y_min=1000000;
%  k=1;
%  i=1;
%  while(k<=18);
%      y_max=0;
%      y_min=1000000;
%      while(i<=25*k);
%          if (y_training(i,1)>y_max);
%              y_max=y_training(i,1);
%          end
%          if (y_training(i,1)<y_min);
%              y_min=y_training(i,1);
%          end
%          i=i+1;  
%      end
%      y_max=repmat(y_max,1,25);
%      Y_max=[Y_max,y_max];
%      y_min=repmat(y_min,1,25);
%      Y_min=[Y_min,y_min];
%      k=k+1;
%  end
% Y_max=Y_max';
% Y_min=Y_min';
%  %maintenant on va multiplier chaque élément de Y_training par
%  %(y_training(i,1)-Y_min)/(Y_max-Y_min)
% 
% i=1;
% Y_training=zeros(18*25,1);
% while (i<=450);
%     Y_training(i,1)=(y_training(i,1)-Y_min(i,1))/(Y_max(i,1)-Y_min(i,1));%y_training(i,1)*(1/Y_max(i,1))
%     i=i+1;
% end
% %creation de X_training
% X=[0:1/24:1];
% X_training=X';
% X_training=repmat(X_training,18,1);

save('training.mat','X_training','Y_training')