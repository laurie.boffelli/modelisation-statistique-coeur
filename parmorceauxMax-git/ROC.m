function [F_mesure,M_seuil]=ROC()
    Seuil=[-9:0.1:1];
    diagn=[true,false,true,false,false,true,false,false,false,false,true,false,true,false,false,true,false,false,false,false,true,false,false,true,true,true,true,false,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true];
    a=1;
    listept=dir(['LLLDyn*.txt']);
    X=zeros(1,47);
    Y=zeros(1,47);
    P=zeros(1,47);
    R=zeros(1,47);
    dist1=2^(1/2);
    F_mesure1=0;
    while Seuil(a)~=1
        seuil=Seuil(a);
        [TN,TP,FN,FP]=TN_TP_FN_FP(listept,seuil,diagn);
        TPR=TP/(TP+FN);
        Y(1,a)=TPR;
        FPR=FP/(FP+TN);
        X(1,a)=FPR;
        dist2=((X(1,a))^2+((Y(1,a))-1)^2)^(1/2);
        %dist1
        %a
        if dist2<dist1
            %a
            dist1=dist2;
            M_seuil=Seuil(a);
        end
        
        P(1,a)=TP/(TP+FP);
        R(1,a)=TP/(TP+FN);
        
        a=a+1;
    end
    [TN,TP,FN,FP]=TN_TP_FN_FP(listept,M_seuil,diagn);
    Prec=TP/(TP+FP);
    Rap=TP/(TP+FN);
    F_mesure=(2*Prec*Rap)/(Prec+Rap)
    TPR=TP/(TP+FN);
    FPR=FP/(FP+TN);
    dist2=(FPR^2+(TPR-1)^2)^(1/2)
    M_seuil
    %Seuil(81);
    plot(X,Y);
    %(1,82);
    %X(1,82);
    
    [X2,I]=sort(X);
    Y2=Y(I);
    len=length(X2);
    diff_X = X2(2:len) - X2(1:len-1);
    H = (Y2(2:len) + Y2(1:len-1)) / 2;
    AUROC = sum(diff_X .* H)
    
    [R2,I]=sort(R);
    P2=P(I);
    len=length(R2);
    diff_R = R2(2:len) - R2(1:len-1);
    H = (P2(2:len) + P2(1:len-1)) / 2;
    AUPR = sum(diff_R .* H)
    
end
%[TN,TP,FN,FP]=TN_TP_FN_FP(listept,-1,diagn)