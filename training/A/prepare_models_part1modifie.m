% build dynamics model (part 1)
%modele A
Y=load('ListeDesVolumes.txt');
y_training=zeros(192,1)%zeros(210,1);
C=xlsread('points courbes.xls')

% on normalise les patients
J=1;
I=1;
ymax_training=Y;
ymax_training=zeros(18,1);
while (I<=18);
    % on cherche le max du patient
    J=1;
    while (J<=25);
        if Y(J,I) > ymax_training(I,1)
            ymax_training(I,1)=Y(J,I);
        end
        J=J+1;
    end
    
    % on normalise le patient
    J=1;
    while (J<=25);
        Y(J,I) = Y(J,I) / ymax_training(I,1);
        J=J+1;
    end
    
    I=I+1;
end

% on récupère les frames qui nous intéressent
J=1;
I=1;
i=1;
while (I<=18);
    J=1;
    while (J<C(I,1));
        y_training(i,1)=Y(J,I);
        i=i+1;
        J=J+1;
    end
    I=I+1;
end
Y_training = y_training;

%creation de X_training
k=1;
X_training=[];
while k<=18
    X=[0:1/(C(k,1)-1):1];
    X_training = [X_training, X];
    k=k+1;
end
X_training=X_training';
%X_training=repmat(X_training,18,1);


save('training.mat','X_training','Y_training')